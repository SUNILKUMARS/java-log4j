package com.lara;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.lara.model.Person;

public class Bootstrap {

	final static Logger logger = Logger.getLogger(Bootstrap.class);
	public static void main(String[] args) 
	{
		/*logger.info("inside main class");
		logger.debug("debug log before setting");
		logger.trace("trace log before setting");
		
//		logger.setLevel(Level.INFO);//we can set class level//it should not avilabale inside java(main ) class
		
		logger.debug("debug log after setting");
		logger.trace("trace log after setting");
		System.out.println("done");
		*/
		Person p = new Person();
		p.setId(123L);
		p.setName("lara");
		
		logger.debug("debug log " +p);
	}
}
